/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyCMD;

import java.io.File;

/**
 *
 * @author cruelkid
 */
public class FileOrDirectoryExample {
    
    public static void createDirectory(String path) {
        File directory = new File(path);
        
        if(directory.exists()) {
            directory.delete();
        } else {
            directory.mkdir();
        }
    }
}
