/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyCMD;

import java.io.BufferedReader;
import java.io.File;
import static java.io.File.separator;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class MyCommandLineExample {
    
    private static String defaultPath = "src" + separator + "MyCMD";
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static String line = null;
    private static String path = defaultPath;
    private static String filePath = null;
        
    public static void runCMD() {
        
        System.out.print(path + ">");
        
        try {
            while((line = reader.readLine()) != null) {
                                
                if(line.equalsIgnoreCase("exit")) break;
                
                if(line.equalsIgnoreCase("gadbf")) {
                    ArrayList<String> list = getAllDirectoriesByFilter();
                    for(int i = 0; i < list.size(); i++) {
                        System.out.println(list.get(i));
                    }
                    String stringList = separator;
                    if(list.size() == 1) {
                        for(String s : list) {
                            stringList += s;
                        }
                        path += stringList;
                    } else { 
                        for(int i = 0; i < list.size(); i++) {
                            if(line.equalsIgnoreCase(list.get(i))) {
                                stringList += list.get(i);
                            }
                        }
                        path += stringList;
                    }
                }
                
                if(containsIgnoreCase(line, "gafbf ")) {
                    ArrayList<String> list = getAllFilesByFilter();
                    for(int i = 0; i < list.size(); i++) {
                        System.out.println(list.get(i));
                    }
                    if(list.size() == 1) {
                        String s = list.get(0);
                        String s2 = s.substring(0, s.indexOf("."));
                        readFile(s2);
                    } else {
                        File file = new File(path);
                        String fName = line.replace("gafbf", "").trim();
                        for(File f : file.listFiles(new FileNamesFilter(fName))) {
                            String tmp = f.getName();
                            String exp = tmp.substring(tmp.indexOf("."));
                            String tmp1 = fName + exp;
                            if(tmp1.equalsIgnoreCase(tmp)) {
                                filePath = path + separator + tmp;
                                readFile(fName);
                                System.out.println();
                            }
                        }
                    }
                }
                
                if(containsIgnoreCase(line, "copy ")) {
                    String name1 = line.substring(line.indexOf(" ") + 1, line.indexOf(","));
                    String name2 = line.substring(line.indexOf(",") + 2);
                    File f1 = new File(path + separator + name1);
                    File f2 = new File(path + separator + name2);
                    copyFile(f1, f2);
                }
                
                if(containsIgnoreCase(line, "rename ")) {
                    String name1 = line.substring(line.indexOf(" ") + 1, line.indexOf(","));                    
                    String name2 = line.substring(line.indexOf(",") + 2);
                    File f1 = new File(path + separator + name1);
                    String tmp = f1.getName();
                    String exp = tmp.substring(tmp.indexOf("."));
                    renameTo(f1, name2 + exp);
                }
                
                if(containsIgnoreCase(line, "mkdir ")) {
                    String stringRes = line.substring(line.indexOf(" ") + 1);
                    mkDir(stringRes);
                } else if(line.equalsIgnoreCase("mkdir")) {
                    mkDir();
                }
                
                if(containsIgnoreCase(line, "fillfile ")) {
                    String res = line.replace("fillfile", "").trim();
                    fillFile(res);
                }
                
                if(containsIgnoreCase(line, "readfile ")) {
                    String res = line.replace("readfile", "").trim();
                    readFile(res);
                }
                
                if(containsIgnoreCase(line, "delete ")) {
                    String str = line.substring(line.indexOf(" ") + 1);
                    deleteSome(str);
                }
                    
                if(line.contains("ls")) {
                    printAllFilesAndDirectories(path);
                } else if(line.contains("cd")) {
                    String pathName = separator + line.replace("cd", "").trim();
                    if(pathName.contains("..")) {
                        File file = new File(path);
                        path = file.getParentFile().getPath();
                    } else {
                    path += pathName;
                    }
                }
                    System.out.print(path + ">");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }    

    private static void printAllFilesAndDirectories(String path) {
        File file = new File(path);
        for(File f : file.listFiles()) {
            System.out.println(f.getName());
        }
    }
    
    public static ArrayList<String> getAllDirectoriesByFilter() {
        ArrayList<String> arr = new ArrayList<>();
        try {
            line = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        File file = new File(path);
        for(File f : file.listFiles()) {
            String tmp = f.getName();
            boolean s = Pattern.compile(Pattern.quote(line), Pattern.CASE_INSENSITIVE).matcher(tmp).find();
            if(s && f.isDirectory() && containsIgnoreCase(f.getName(), line)) {
                arr.add(f.getName());
            } else if (s && f.isDirectory() && f.getName().equalsIgnoreCase(line)) {
                arr.add(f.getName());
            }
        }
        return arr;
    }
    
    public static void mkDir() {
        try {
            line = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if(line.contains(".")) {
            File file = new File(path + separator + line);
            try {
                file.createNewFile();            
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            File folder = new File(path + separator + line);
            folder.mkdir();
        }
    }
    
    public static void mkDir(String s) {
        if(s.contains(".")) {
            File file = new File(path + separator + s);
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            File folder = new File(path + separator + s);
            folder.mkdir();
        }
    }
    
    public static void fillFile(String s) throws IOException {
        System.out.println("Enter text to be written:");
        File file = new File(path);
        for(File f : file.listFiles(new FileNamesFilter(s))) {
            String tmp = f.getName();
            filePath = path + separator + tmp;
        }
        String text = reader.readLine();
        
        FileWriter writer = new FileWriter(filePath, true);
        writer.write(text + "\n");
        writer.flush();
    }
    
    public static void readFile(String s) throws IOException {
        File file = new File(path);
        for(File f : file.listFiles(new FileNamesFilter(s))) {
            String tmp = path + separator + f.getName();
            String exp = tmp.substring(tmp.indexOf("."));
            String fName = path + separator + s + exp;
            filePath = tmp;
            if(fName.equalsIgnoreCase(tmp)) {
                FileReader readFile = new FileReader(filePath);
        
                int c;
                while((c = readFile.read()) != -1) {
                     System.out.print((char)c);
                }
            }
        }
        //System.out.println();
    }
    
    public static void deleteSome(String s) {
            File file = new File(path + separator + s);
            file.delete();
    }
    
    public static void copyFile(File f1, File f2) {
        if(f1.getName().equals(f2.getName())) {
            System.out.println("ERROR! File names can't be equals!");
        } else {
            try {
                Files.copy(f1.toPath(), f2.toPath());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public static void renameTo(File f1, String reName) {
        File fReName = new File(path + separator + reName);
        f1.renameTo(fReName);
    }
    
    public static ArrayList<String> getAllFilesByFilter() {
        ArrayList<String> arr = new ArrayList<>();
        File file = new File(path);
        String fName = line.replace("gafbf", "").trim();
        for(File f : file.listFiles(new FileNamesFilter(fName))) {
            String tmp = f.getName();
            String exp = tmp.substring(tmp.indexOf("."));
                if(containsIgnoreCase(tmp + exp, fName)) {
                    arr.add(f.getName());
                } else if (f.getName().equalsIgnoreCase(fName + exp)) {
                    arr.add(f.getName());
                }
        }
        return arr;
    }
    
    public static boolean containsIgnoreCase(String s1, String s2) {
        return (s1.toLowerCase().contains(s2.toLowerCase()));
    }
}
