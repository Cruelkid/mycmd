/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyCMD;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author cruelkid
 */
class FileNamesFilter implements FileFilter {
    
    private String fileName;
    
    public FileNamesFilter(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean accept(File file) {
        return file.isFile() && containsIgnoreCase(file.getName(), fileName);
    }
    
    public static boolean containsIgnoreCase(String s1, String s2) {
        return (s1.toLowerCase().contains(s2.toLowerCase()));
    }
    
}
