/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyCMD;

/**
 *
 * @author (s)AINT
 */
public class Student implements Comparable<Student> {

    private String firstName;
    private String lastName;
    private int age;
    private long id;

    public Student() {
    }

    public Student(String firstName, long id) {
        this.firstName = firstName;
        this.id = id;
    }

    public Student(String firstName, String lastName, int age, long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return id == student.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", id=" + id +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        Student entry = (Student) o;
            
             int result = firstName.compareTo(entry.firstName);
             if(result != 0) {
                    return result;
             }
            
             result = age - entry.age;
             if(result != 0) {
                    return (int) result / Math.abs( result );
             }
             return 0;
    }
}
