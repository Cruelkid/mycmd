/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyCMD;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cruelkid
 */
public class BufferExample {
    
    public static void writeBufferToFile(String data, File file) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            
            for(String s : data.split("\\.")) {
                writer.write(s);
            }
            
            writer.close();
                    
        } catch (IOException ex) {
            Logger.getLogger(BufferExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
